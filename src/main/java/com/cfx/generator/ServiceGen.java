package com.cfx.generator;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import javax.lang.model.element.Modifier;

import com.cfx.service.api.Service;
import com.cfx.service.api.ServiceException;
import com.cfx.service.api.StartContext;
import com.cfx.service.api.StopContext;
import com.cfx.service.api.config.Configuration;
import com.google.common.base.CaseFormat;
import com.google.common.reflect.ClassPath;
import com.google.common.reflect.ClassPath.ClassInfo;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.TypeSpec.Builder;

import io.macaw.test.impl.ServiceConfig;
import ru.vyarus.java.generics.resolver.GenericsResolver;
import ru.vyarus.java.generics.resolver.context.GenericsContext;
import ru.vyarus.java.generics.resolver.context.MethodGenericsContext;

public class ServiceGen {
    
    private final String sourcePackage;
    private final File outputLocation;
    private final String namespace;
    private final String servicename;
    
    private final Map<String, Class<?>> classes = new HashMap<String, Class<?>>();
    private final Map<String, Class<?>> serviceClasses = new HashMap<String, Class<?>>();
    private static final Map<String, String> TYPE_MAPPING = getTypeMapping();
    
    public ServiceGen(String sourcePackage, File outputLocation, String namespace, String servicename) {
        this.outputLocation = outputLocation;
        this.sourcePackage = sourcePackage;
        this.namespace = namespace;
        this.servicename = servicename;
    }
    
    public void generate() throws IOException {
        ClassLoader loader = getClass().getClassLoader();
        ClassPath cp=ClassPath.from(loader);
        for ( ClassInfo ci : cp.getTopLevelClassesRecursive(sourcePackage) ) {
            Class<?> aClass = ci.load();
            classes.put(ci.getName(), aClass);
            if (isService(aClass)) {
                serviceClasses.put(ci.getName(), aClass);
            }
        }

        JsonObjectBuilder serviceDescriptorBuilder = null;
        JsonObjectBuilder serviceBuilder = null;
        JsonArrayBuilder apisBuilder = null;
        TypeSpec.Builder serviceImplBuilder = null;
        TypeSpec.Builder serviceClientBuilder = null;
        String packageName = null;
        if (servicename != null) {
            packageName = namespace + ".macaw";
            serviceDescriptorBuilder = Json.createObjectBuilder();
            serviceBuilder = generateService(servicename,
                    servicename, namespace, packageName);            
            apisBuilder = Json.createArrayBuilder();
            serviceImplBuilder = generateServiceImpl(packageName, CaseFormat.LOWER_HYPHEN.to(CaseFormat.UPPER_CAMEL, servicename));
        }

        for (Entry<String, Class<?>> entry : serviceClasses.entrySet()) {
            Class<?> sClass = entry.getValue();
            System.out.println("Generating Service : " + entry.getKey());
            if (servicename == null) {
                packageName = sClass.getPackage().getName() + ".macaw";
                serviceClientBuilder = generateServiceClient(sClass, packageName, sClass.getSimpleName());
                serviceImplBuilder = generateServiceImpl(packageName, sClass.getSimpleName());   
                serviceDescriptorBuilder = Json.createObjectBuilder();
                serviceBuilder = generateService(entry.getValue(), packageName, serviceImplBuilder, serviceClientBuilder, sClass.getSimpleName());
                serviceDescriptorBuilder.add("service", serviceBuilder.build());
                JsonObject serviceDescriptor = serviceDescriptorBuilder.build();
                save(packageName + ".impl", getServiceName(entry.getValue()), serviceDescriptor, serviceImplBuilder, Collections.singletonList(sClass));
                saveClient(getServiceName(sClass), serviceClientBuilder, sClass.getPackage().getName());
            }
            else {
                serviceClientBuilder = generateServiceClient(sClass, packageName + ".impl", servicename);
                generateApis(sClass, apisBuilder, serviceImplBuilder, serviceClientBuilder, servicename);
                saveClient(servicename, serviceClientBuilder, sClass.getPackage().getName());
            }
        }
        if (servicename != null) {
            serviceBuilder.add("apis", apisBuilder.build());
            serviceDescriptorBuilder.add("service", serviceBuilder.build());
            JsonObject serviceDescriptor = serviceDescriptorBuilder.build();
            save(packageName + ".impl", servicename, serviceDescriptor, serviceImplBuilder, serviceClasses.values());
        }
    }
    
    private void saveClient(String servicename, TypeSpec.Builder serviceClientBuilder, String clientPackageName) throws IOException {
        String svcOutputLocation = outputLocation.getAbsolutePath() + File.separatorChar + servicename;
        String svcClientLocation = svcOutputLocation + File.separatorChar + "client";
        File svcClientFolder = new File(svcClientLocation);
        if ( ! svcClientFolder.exists() ) {
            svcClientFolder.mkdirs();
        }
        
        JavaFile javaFile = JavaFile.builder(clientPackageName, serviceClientBuilder.build()).build();
        javaFile.writeTo(svcClientFolder);
    }
    
    private void save(String packageName, String servicename, JsonObject serviceDescriptor, TypeSpec.Builder serviceImplBuilder, Collection<Class<?>> services) throws IOException {
        Map<String, Object> properties = new HashMap<String, Object>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);        
        JsonWriter jsonWriter = null;
        PrintWriter writer = null;
        try {
            String svcOutputLocation = outputLocation.getAbsolutePath() + File.separatorChar + servicename;
            String svcDesignLocation = svcOutputLocation + File.separatorChar + "design";
            File svcOutputFolder = new File(svcDesignLocation);
            if ( ! svcOutputFolder.exists() ) {
                svcOutputFolder.mkdirs();
            }
            writer = new PrintWriter( svcOutputFolder.getAbsolutePath() + File.separatorChar + servicename + ".json");
            JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
            jsonWriter = writerFactory.createWriter(writer);
            jsonWriter.writeObject(serviceDescriptor);
            jsonWriter.close();
            
            Properties genProperties = new Properties();
            genProperties.setProperty("project.build.tool","maven");
            genProperties.setProperty("source.model.type","json");
            genProperties.setProperty("macaw.platform.version","0.9.5-CR1");
            genProperties.setProperty("artifacts.output.dir","../..");
            genProperties.setProperty("input.files.dir",".");
            genProperties.setProperty("project.dist.organization.name","io.macaw");
            genProperties.setProperty("module.version","1.0.0");
            genProperties.setProperty("project.impl.language","java");
            genProperties.setProperty("service.gen.option.generate.impl.artifacts","false");            
            writer = new PrintWriter( svcOutputFolder.getAbsolutePath() + File.separatorChar + "service-artifacts-gen.properties");
            genProperties.store(writer, "Service artifact generator properties");
            writer.close();
            
            JavaFile javaFile = JavaFile.builder(packageName, serviceImplBuilder.build()).build();
            javaFile.writeTo(svcOutputFolder);
            
            Builder appBuilder = generateServiceApp(packageName, servicename, services);
            javaFile = JavaFile.builder(packageName, appBuilder.build()).build();
            javaFile.writeTo(svcOutputFolder);
        }
        finally {
            if (jsonWriter != null) {
                jsonWriter.close();
            }
            if (writer != null) {
                writer.close();
            }
        }
    }
    
    private JsonObjectBuilder generateService(Class<?> sClass, String packageName, TypeSpec.Builder serviceImplBuilder, TypeSpec.Builder serviceClientBuilder, String targetServiceName) {        
        JsonObjectBuilder serviceBuilder = generateService(getServiceName(sClass),
                sClass.getName(), namespace, packageName);
        
        JsonArrayBuilder apisBuilder = Json.createArrayBuilder();
        generateApis(sClass, apisBuilder, serviceImplBuilder, serviceClientBuilder, targetServiceName);
        serviceBuilder.add("apis", apisBuilder.build());
        return serviceBuilder;
    }
    
    private JsonObjectBuilder generateService(String name, String description, String namespace, String packageName) {
        JsonObjectBuilder serviceBuilder = Json.createObjectBuilder();
        serviceBuilder.add("name", name);
        serviceBuilder.add("description", description);
        serviceBuilder.add("version", "1.0.0");
        serviceBuilder.add("namespace", namespace);
        
        JsonObjectBuilder javaBuilder = Json.createObjectBuilder();
        javaBuilder.add("package-name", packageName);
        JsonObjectBuilder codegenBuilder = Json.createObjectBuilder();
        codegenBuilder.add("java", javaBuilder.build());
        JsonObjectBuilder optionsBuilder = Json.createObjectBuilder();
        optionsBuilder.add("code-gen", codegenBuilder.build());
        serviceBuilder.add("options", optionsBuilder.build());        
        return serviceBuilder;
    }

    private void generateApis(Class<?> sClass, JsonArrayBuilder apisBuilder, TypeSpec.Builder serviceImplBuilder, TypeSpec.Builder serviceClientBuilder, String targetServiceName) {
        Map<String, Integer> methodCounts = new HashMap<>();
        for (Method method : sClass.getMethods()) {
            if (ignore(method)) {
                continue;
            }
            JsonObjectBuilder apiBuilder = generateAPI(sClass, method, methodCounts, serviceImplBuilder, serviceClientBuilder, targetServiceName);
            apisBuilder.add(apiBuilder.build());
        }
    }
    
    private boolean ignore(Method method) {
        if (method.getDeclaringClass().equals(Object.class)) {
            return true;
        }
        return false;
    }
    
    private JsonObjectBuilder generateAPI(Class<?> sClass, Method method, Map<String, Integer> methodCounts, TypeSpec.Builder serviceImplBuilder, TypeSpec.Builder serviceClientBuilder, String targetServiceName) {
        GenericsContext context = GenericsResolver.resolve(sClass);
        MethodGenericsContext methodContext = context.method(method);
        JsonObjectBuilder apiBuilder = Json.createObjectBuilder();
        String apiName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_HYPHEN, method.getName());
        Integer count = methodCounts.get(apiName); 
        if (count == null) {
            count = 0;
            methodCounts.put(apiName, count);
        }
        else {
            count++;
            methodCounts.put(apiName, count);
            apiName = apiName + count;
        }
        
        if (this.servicename != null) {
            String prefix = sClass.getSimpleName().toLowerCase();
            if (prefix.endsWith("serviceimpl")) {
                prefix = prefix.replaceAll("serviceimpl", "");
            }
            apiName = prefix + "-" + apiName;
        }
        
        MethodSpec.Builder methodImplBuilder = MethodSpec.methodBuilder(CaseFormat.LOWER_HYPHEN.to(CaseFormat.LOWER_CAMEL, apiName)).addModifiers(Modifier.PUBLIC);;
        MethodSpec.Builder methodClientBuilder = MethodSpec.methodBuilder(method.getName()).addModifiers(Modifier.PUBLIC);
        
        System.out.println("\tGenerating API : " + apiName);
        apiBuilder.add("name", apiName);
        apiBuilder.add("description", apiName);
        Parameter[] params = method.getParameters();
        StringBuffer paramsBuf = null;
        Parameter inOutParam = null;
        Class<?> inOutParamType = null;
        if (params != null && params.length > 0) {
            if ( ! isVoid(params[0].getType())) {
                JsonArrayBuilder inputsBuilder = Json.createArrayBuilder();
                List<Class<?>> paramTypes = methodContext.resolveParameters();
                for (int i = 0; i < params.length; i++) {
                    if (isInOut(params[i])) {
                        inOutParam = params[i];
                        inOutParamType = paramTypes.get(i);
                    }
                    if (paramsBuf == null) {
                        paramsBuf = new StringBuffer(params[i].getName());
                    }
                    else {
                        paramsBuf.append(',').append(params[i].getName());
                    }
                    methodImplBuilder.addParameter(paramTypes.get(i), params[i].getName());
                    methodClientBuilder.addParameter(paramTypes.get(i), params[i].getName());
                    JsonObjectBuilder paramBuilder = generateParameter(sClass, method, paramTypes.get(i).getTypeName(), params[i].getType(), params[i]);
                    inputsBuilder.add(paramBuilder.build());            
                }
                apiBuilder.add("inputs", inputsBuilder.build());            
            }
        }
        
        String returnPrefix = "";
        
        Class<?> returnClass = methodContext.resolveReturnClass();
        if (isVoid(returnClass)) {
            methodClientBuilder.returns(void.class);
        }
        String returnInOut = null;
        if (inOutParam != null) {
            returnClass = inOutParamType;
            returnInOut = "return " + inOutParam.getName();
        }
        if (isVoid(returnClass)) {
            methodImplBuilder.returns(void.class);
        }
        else {
            JsonObjectBuilder returnBuilder = generateParameter(sClass, method, returnClass.getName(), returnClass, null);
            methodImplBuilder.returns(returnClass);
            if (inOutParam == null) {
                returnPrefix = "return ";
                methodClientBuilder.returns(returnClass);
            }
            apiBuilder.add("output", returnBuilder.build());            
        }
        
        String serviceFieldName = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, sClass.getSimpleName());
        if (serviceFieldName.endsWith("Impl")) {
            serviceFieldName = serviceFieldName.replace("Impl","");
        }
        
        String appName = getAppName(serviceFieldName);
        if (this.servicename != null) {
            appName = getAppName(this.servicename);
        }
        serviceFieldName = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL,appName) + "." + serviceFieldName;
        String paramsStr = paramsBuf == null ? "" : paramsBuf.toString();
        boolean generateTryBlock = false;
        if (method.getExceptionTypes().length > 0) {
            JsonObjectBuilder exceptionsBuilder = Json.createObjectBuilder();
            generateTryBlock = generateExceptions(exceptionsBuilder, method, methodImplBuilder, methodClientBuilder);
            apiBuilder.add("exceptions", exceptionsBuilder.build());            
        }
        if (generateTryBlock) {
            returnInOut = (returnInOut == null) ? "" : "    " + returnInOut + ";\n";
            methodImplBuilder.addCode(""
                    + "try {\n"
                    + "    " + String.format("%s%s.%s(%s);\n", returnPrefix, serviceFieldName, method.getName(), paramsStr)
                    + returnInOut
                    + "}\n"
                    + "catch (Exception e) {\n"
                    + "    throw new RuntimeException(e);\n"
                    + "}\n"
                    );
        }
        else {
            methodImplBuilder.addStatement(String.format("%s%s.%s(%s)", returnPrefix, serviceFieldName, method.getName(), paramsStr));
            if (returnInOut != null) {
                methodImplBuilder.addStatement(returnInOut);
            }
        }
        
        String targetApiName = CaseFormat.LOWER_HYPHEN.to(CaseFormat.LOWER_CAMEL, apiName);
        
        targetServiceName = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, targetServiceName);
        
        if (inOutParam == null) {
            methodClientBuilder.addStatement(String.format("%s%s.%s(%s)", returnPrefix, targetServiceName, targetApiName, paramsStr));
        }
        else {
            //assign return value to local and if not null copy it to the inout param
            //
            methodClientBuilder.addCode(""
                    + "try {\n"
                    + String.format("    Object out = %s.%s(%s);\n", targetServiceName, targetApiName, paramsStr)
                    + "    if (out != null)  org.apache.commons.beanutils.PropertyUtils.copyProperties(" + inOutParam.getName() + ", out);\n"
                    + "}\n"
                    + "catch (Exception e) {\n"
                    + "    throw new RuntimeException(e);\n"
                    + "}\n"
                    );
        }        
        serviceClientBuilder.addMethod(methodClientBuilder.build());
        serviceImplBuilder.addMethod(methodImplBuilder.build());
        return apiBuilder;
    }

    private boolean isVoid(Class<?> aClass) {
        return aClass == null || Void.class.equals(aClass) || void.class.equals(aClass);
    }
    
    private boolean generateExceptions(JsonObjectBuilder exceptionsBuilder, Method method, MethodSpec.Builder methodImplBuilder, MethodSpec.Builder methodClientBuilder) {
        boolean hasJavaExceptions = false;
        for (Class<?> type : method.getExceptionTypes()) {
            if (type.getPackage().getName().startsWith("java")) {
                hasJavaExceptions = true;
                continue;
            }
            else {
                methodImplBuilder.addException(type);
                methodClientBuilder.addException(type);
                
                JsonObjectBuilder exBuilder = Json.createObjectBuilder();
                JsonObjectBuilder javaBuilder = Json.createObjectBuilder();
                javaBuilder.add("package-name", type.getPackage().getName());
                JsonObjectBuilder codegenBuilder = Json.createObjectBuilder();
                codegenBuilder.add("java", javaBuilder.build());
                JsonObjectBuilder optionsBuilder = Json.createObjectBuilder();
                optionsBuilder.add("code-gen", codegenBuilder.build());
                exBuilder.add("options", optionsBuilder.build());
                exceptionsBuilder.add(type.getSimpleName(), exBuilder.build());
            }
        }
        return hasJavaExceptions;
    }
    
    private JsonObjectBuilder generateParameter(Class<?> sClass, Method method, String actualType, Class<?> pClass, Parameter param) {
        JsonObjectBuilder paramBuilder = Json.createObjectBuilder();            

        if (param != null) {
            paramBuilder.add("name", param.getName()/*CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_HYPHEN, param.getName())*/);
        }
        generateType(pClass, actualType, paramBuilder);
        return paramBuilder;
    }
    
    private void generateType(Class<?> pClass, String actualType, JsonObjectBuilder paramBuilder) {
        String mappedType = TYPE_MAPPING.get(actualType);
        String actualTypeName = mappedType;
        if (mappedType == null) {
            paramBuilder.add("external-type", "java-type-ref");
            actualTypeName = actualType;
        }
        if (pClass.isArray()) {
            paramBuilder.add("type", "array");
            paramBuilder.add("item", actualTypeName);
        }
        else if (pClass.isAssignableFrom(List.class)) {
            if (mappedType == null) {
                paramBuilder.add("type", actualTypeName);
            }
            else {
                paramBuilder.add("type", "list");
                paramBuilder.add("item", actualTypeName);
            }
        }
        else {
            paramBuilder.add("type", actualTypeName);
        }
    }
    
    private String getServiceName(Class<?> sClass) {
        String name = sClass.getSimpleName();
        if (name.toLowerCase().endsWith("impl") && name.length() > 4) {
            name = name.substring(0, name.length() - 4);
        }
        name = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_HYPHEN, name);
        if (name.endsWith("-")) {
            name = name.substring(0, name.length() - 2);
        }
        return name;
    }
    
    private boolean isService(Class<?> aClass) {
        for ( Annotation annotation : aClass.getAnnotations() ) {
            if (annotation.annotationType().getName().endsWith("Service")) {
                return true;
            }
        }
        return false;
    }
    
    private boolean isInOut(Parameter inOutParam) {
        for ( Annotation annotation : inOutParam.getAnnotations() ) {
            if (annotation.annotationType().getName().endsWith("InOut")) {
                return true;
            }
        }
        return false;
    }

    private static Map<String, String> getTypeMapping()
    {
        Map<String, String> ret = new HashMap<String, String>();
        ret.put(boolean.class.getTypeName(), "boolean");
        ret.put(char.class.getTypeName(), "byte");
        ret.put(byte.class.getTypeName(), "byte");
        ret.put(short.class.getTypeName(), "int32");
        ret.put(int.class.getTypeName(), "int32");
        ret.put(long.class.getTypeName(), "int64");
        ret.put(float.class.getTypeName(), "decimal64");
        ret.put(double.class.getTypeName(), "decimal64");
        ret.put(String.class.getTypeName(), "string");
        ret.put(Date.class.getTypeName(), "date");
        return ret;
    }
    
    private Builder generateServiceImpl(String packageName, String className) { 
        if (className.endsWith("Impl")) {
            className = className.replaceAll("Impl", "");
        }
        
        Builder builder = TypeSpec.classBuilder(className).addModifiers(Modifier.PUBLIC);
        builder.addSuperinterface(Service.class);
        ClassName serviceIf = ClassName.get(packageName, className);
        builder.addSuperinterface(serviceIf);
        ClassName serviceApp = ClassName.get(packageName + ".impl", className + "App");
        ClassName appContext = ClassName.get("org.springframework.context", "ConfigurableApplicationContext");
        
        String appFieldName = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, className + "App");
        FieldSpec appField = FieldSpec.builder(serviceApp, appFieldName, Modifier.PRIVATE).build();
        builder.addField(appField);
        
        MethodSpec initialize = MethodSpec.methodBuilder("initialize")
                .addModifiers(Modifier.PUBLIC)
                .returns(void.class)
                .addParameter(Configuration.class, "config")
                .addException(ServiceException.class)
                .build();
        MethodSpec start = MethodSpec.methodBuilder("start")
                .addModifiers(Modifier.PUBLIC)
                .returns(void.class)
                .addParameter(StartContext.class, "startContext")
                .addException(ServiceException.class)
                .addStatement("$T ctx = org.springframework.boot.SpringApplication.run($T.class, new String[0])", appContext, serviceApp )
                .addStatement("$L = ctx.getBean($T.class)", appFieldName, serviceApp )
                .build();
        MethodSpec stop = MethodSpec.methodBuilder("stop")
                .addModifiers(Modifier.PUBLIC)
                .returns(void.class)
                .addParameter(StopContext.class, "stopContext")
                .addException(ServiceException.class)
                .build();
        
        builder.addMethod(initialize);
        builder.addMethod(start);
        builder.addMethod(stop);
        return builder;
    }    
    
    private String getAppName(String className) {
        className = CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, className);
        return className + "App";
    }
    
    private String getClientName(String className) {
        className = CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, className);
        return className + "Client";
    }
    
    private Builder generateServiceClient(Class<?> sClass, String targetPackageName, String targetServiceNameSpec) {
        
        String targetServiceName = CaseFormat.LOWER_HYPHEN.to(CaseFormat.UPPER_CAMEL, targetServiceNameSpec);
        
        String clientName = sClass.getSimpleName();
        if (clientName.endsWith("Impl")) {
            clientName = clientName.replaceAll("Impl", "");
        }
        
        if (targetPackageName.endsWith(".impl")) {
            targetPackageName = targetPackageName.replaceAll(".impl", "");
        }
        
        ClassName ifType = ClassName.get(sClass.getPackage().getName(), clientName);
        
        clientName = getClientName(clientName);
        Builder builder = TypeSpec.classBuilder(clientName).addModifiers(Modifier.PUBLIC);      
        ClassName svcAnnotation = ClassName.get("org.springframework.stereotype", "Service");
        builder.addAnnotation(svcAnnotation);        
        builder.addSuperinterface(ifType);        
        
        ClassName targetType = ClassName.get(targetPackageName, targetServiceName);
                
        String fieldName = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, targetServiceName);
        
        FieldSpec fieldSpec = FieldSpec.builder(targetType, fieldName, Modifier.PRIVATE).build();
        builder.addField(fieldSpec);
        
        MethodSpec clientInit = MethodSpec.constructorBuilder()
                .addModifiers(Modifier.PUBLIC)
                .addStatement("$L = $T.getInstance().locateService($T.class, $S, $S)", fieldName, ServiceConfig.class, targetType, this.namespace, targetServiceNameSpec)
                .build();
        
        builder.addMethod(clientInit);
        
        return builder;
    }    
    
    private Builder generateServiceApp(String packageName, String className, Collection<Class<?>> services) {
        className = getAppName(className);
        Builder builder = TypeSpec.classBuilder(className).addModifiers(Modifier.PUBLIC);;        
        ClassName sbApp = ClassName.get("org.springframework.boot.autoconfigure", "SpringBootApplication");
        builder.addAnnotation(sbApp);
        
        for (Class<?> service : services) {
            
            String serviceName = service.getSimpleName();
            if (serviceName.endsWith("Impl")) {
                serviceName = serviceName.replaceAll("Impl", "");
            }
            String fieldName = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, serviceName);
            
            ClassName svcField = ClassName.get(service.getPackage().getName(), serviceName);
            
            ClassName inject = ClassName.get("javax.inject", "Inject");
            FieldSpec fieldSpec = FieldSpec.builder(svcField, fieldName, Modifier.PUBLIC).addAnnotation(inject).build();
            builder.addField(fieldSpec);
        }
        
        return builder;
    }    
}
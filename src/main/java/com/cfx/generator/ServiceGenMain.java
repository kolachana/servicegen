package com.cfx.generator;

import java.io.File;
import java.net.MalformedURLException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class ServiceGenMain {

    public static void main(String[] args) throws Exception {
        Options options = new Options();
        options.addOption("h", "help", false, "Show help.");
        options.addOption("p", "package", true,
                "Specify the source package containing classes with spring Service annotation");
        options.addOption("o", "output", true,
                "Specify the output location for generated service(s)");
        options.addOption("ns", "namespace", true,
                "Specify the default namespace of the generated service(s)");
        options.addOption("s", "single", true,
                "Specify name a service if service methods from all classes with service annotation need to be consolidating in one service generated");
        ServiceGen generator = parse(options, args);
        generator.generate();
    }

    public static ServiceGen parse(Options options, String[] args) throws MalformedURLException {
        CommandLineParser parser = new DefaultParser();

        CommandLine cmd = null;
        String sourcePackage = null;
        File output = null;
        String namespace = null;
        String servicename = null;
        try {
            
            cmd = parser.parse(options, args);

            if (cmd.hasOption("h"))
                help(options);
            
            if (cmd.hasOption("p")) {
                sourcePackage = cmd.getOptionValue("p");
            } else {
                System.out.println("Missing package option");
                help(options);
            }
            
            if (cmd.hasOption("o")) {
                output = new File(cmd.getOptionValue("o"));
            } else {
                System.out.println("Missing file option");
                help(options);
            }

            if (cmd.hasOption("ns")) {
                namespace = cmd.getOptionValue("ns");
                if (namespace == null || namespace.trim().isEmpty()) {
                    System.out.println("Missing service namespace. Namespace should conform to Java package naming convention");
                    help(options);
                }
            } else {
                System.out.println("Missing namespace option. Namespace should conform to Java package naming convention");
                help(options);
            }
            
            if (cmd.hasOption("s")) {
                servicename = cmd.getOptionValue("s");
                if (servicename == null || servicename.trim().isEmpty()) {
                    System.out.println("Missing service name");
                    help(options);
                }
            }

        } catch (ParseException e) {
            System.out.println("Failed to parse comand line arguments " + e.getLocalizedMessage());
            help(options);
        }
        System.out.println(String.format("Generating service with the specified parameters Package: %s, Output Location: %s, Namespace: %s, Service: %s",
                sourcePackage, output, namespace, servicename == null 
                ));
        ServiceGen generator = new ServiceGen(sourcePackage, output, namespace, servicename);
        return generator;
    }

    private static void help(Options options) {
        // This prints out some help
        HelpFormatter formater = new HelpFormatter();

        formater.printHelp("Main", options);
        System.exit(0);
    }
}